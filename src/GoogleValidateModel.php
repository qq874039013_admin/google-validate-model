<?php


namespace googleValidateModel;


class GoogleValidateModel
{

    private static $instance = null;
    private $googleValidate = null;

    private function __construct()
    {
        $googleValidate = new GoogleValidate();
    }

    public static function getInstance()
    {
        //检测当前类属性$instance是否已经保存了当前类的实例
        if (self::$instance == null) {
            //如果没有,则创建当前类的实例
            self::$instance = new self();
        }
        //如果已经有了当前类实例,就直接返回,不要重复创建类实例
        return self::$instance;
    }

    /**
     * 设置配置项 ,验证码的长度,secret 长度
     * @param int $codeLength
     * @param int $secretLength
     */
    public function set($codeLength = 6, $secretLength = 16)
    {
        $this->googleValidate->setCodeLength($codeLength);
        $this->googleValidate->setSecretLength($secretLength);
    }

    public function loginValidate($code, $secret)
    {
        return $this->googleValidate->verifyCode($secret, $code, 2);
    }

    /**
     * 创建secret
     * @param $username
     */
    public function createSecret($username)
    {
        $newSecret = $this->googleValidate->createSecret();
        $this->googleValidate->getQRCodeGoogleUrl($username, $newSecret);
    }

    public function changePwd($username, $code, $originSecret)
    {
        if ($this->loginValidate($code, $originSecret)) {
            $this->createSecret($username);
        } else {
            throw new \Exception('code is error');
        }
    }


}